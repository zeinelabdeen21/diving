<?php

use App\Models\Room;
use App\Models\Message;
use App\Models\Room_user;
use App\Models\Message_notification;


/**************************  Start Chat ***************************/
if(!function_exists('creatRoom')){
    function creatRoom($user_id,$type,$order=null){

        // creat new room
        $newRoom = Room::firstOrCreate([
            'type'      => $type,
            'user_id'   => $user_id,
            'order_id'  => $order != null ? $order->id:null
        ]);

        // join owner to room users
        joinRoom($newRoom->id,$user_id);


        // if type order, join provider to room users
        // if($order != null){
        //     $order->room_id = $newRoom->id;
        //     $order->save();

        //     joinRoom($newRoom->id,$order->provider_id);
        // }
        return $newRoom;
    }
}
if(!function_exists('creatPrivateRoom')){
    function creatPrivateRoom($user_id,$otherUser_id){
        $room = Room::where(['private'=>1,'user_id'=>$user_id,'other_user_id'=>$otherUser_id])->first();
        !$room? $room = Room::where(['private'=>1,'other_user_id'=>$user_id,'user_id'=>$otherUser_id])->first():"";
        if(!$room){
            // creat new room
            $room = Room::Create([
                'type'          => 'private',
                'private'       => 1,
                'user_id'       => $user_id,
                'other_user_id' => $otherUser_id,
            ]);

            // join user and partner to room users
            joinRoom($room->id,$user_id);
            joinRoom($room->id,$otherUser_id);
        }
        return $room;
    }
}
if(!function_exists('joinRoom')){
    function joinRoom($room_id,$user_id){
        Room_user::firstOrCreate(['room_id' => $room_id, 'user_id' => $user_id]);
    }
}
if(!function_exists('leaveRoom')){
    function leaveRoom($room_id,$user_id){
        Room_user::where(['room_id' => $room_id, 'user_id' => $user_id])->delete();
    }
}
if(!function_exists('getRoomMessages')){
    function getRoomMessages($room_id,$user_id){
        $room = Room::whereId($room_id)->first();
        if($room){
            $roomMessages = $room->Messages()->with('Message')->where('user_id',$user_id)->where('is_delete',0)->get();
            return $roomMessages;
        }
        return [];
    }
}
if(!function_exists('unreadRoomMessages')){
    function unreadRoomMessages($room_id,$user_id){
       return getRoomMessages($room_id,$user_id)->where('is_seen',0);
    }
}
if(!function_exists('readAllRoomMessages')){
    function readAllRoomMessages($room_id,$user_id){
        $messages = unreadRoomMessages($room_id,$user_id);
        if(count($messages) > 0){
            foreach($messages as $msg){
                $msg->is_seen = 1;
                $msg->save();
            }
        }
    }
}
// save message and make copy from it to every room member
// return msg with other room users socket id
if(!function_exists('saveMessage')){
    function saveMessage($room_id,$message,$sender_id,$type='text'){
        $room = Room::whereId($room_id)->first();
        $lastMessage = "";
        if($room && count($room->Users) > 0){
            // create original message
            $newMessage = new Message;
            $newMessage->body       = $message; // if message is file
            $newMessage->room_id    = $room_id;
            $newMessage->user_id    = $sender_id;
            $newMessage->type       = $type;
            $newMessage->save();

            // update for sort by last message
            $room->last_message_id = $newMessage->id;
            $room->save();

            // create message relation for every room users
            foreach($room->Users as $user){

                $newMessageNoti = new Message_notification;
                $newMessageNoti->message_id = $newMessage->id;
                $newMessageNoti->room_id    = $room_id;
                $newMessageNoti->user_id    = $user->id;

                $newMessageNoti->flagged    = 0;
                $newMessageNoti->is_delete  = 0;

                $newMessageNoti->is_seen    = $user->id == $sender_id ? 1:($user->online == 1?1:0);
                $newMessageNoti->is_sender  = $user->id == $sender_id? 1:0;
                //$newMessageNoti->created_at = date("Y-m-d H:i:s");

                $newMessageNoti->save();
                if($user->id == $sender_id)
                    $lastMessage = $newMessageNoti;

            }
        }
        $lastMessage = Message_notification::where('id',$lastMessage->id)->with('Message')->first();
        //$users = $room->Users->where('id','!=',$sender_id)->pluck('id');
        $users = $room->Users->where('id','!=',$sender_id)->pluck('socket_id')->filter();
        $lastMessage['other_users'] = $users;
        return $lastMessage;
    }
}
if(!function_exists('deleteMessage')){
    function deleteMessage($message_id,$user_id){
        $userMessage = Message_notification::whereId($message_id)->first();
        if($userMessage && $userMessage->user_id == $user_id){
            $userMessage->is_delete = 1;
            $userMessage->save();
        }
    }
}
/**************************  End Chat ***************************/
