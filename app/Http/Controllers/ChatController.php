<?php

namespace App\Http\Controllers;


use App\Models\Room;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class ChatController extends Controller
{

    public function __construct()
    {

    }

    ############################# CHAT WORK #############################
    public function PublicChat()
    {
        return view('admin.publicChat');
    }
    public function NewPrivateRoom(User $user)
    {
        $currentUser = Auth::user();
        $otherUser = $user;
        $room = creatPrivateRoom($currentUser->id, $otherUser->id);
        $messages = getRoomMessages($room->id, $currentUser->id);
        ;
        return response()->json(['status' => 1, 'message' => 'success', 'room' => $room, 'messages' => $messages]);
    }





    public function OtherUsers()
    {
        return response()->json(['status' => 1, 'message' => 'success', 'data' => User::where('id', '!=', Auth::id())->get(['id', 'name', 'socket_id', 'online'])]);
    }
    public function UserRooms()
    {
        $user = Auth::user();
        if ($user) {
            $rooms = $user->Rooms;
            dd($rooms);
            return view('site/rooms', compact('rooms', 'user'));
        }
        return redirect('/');
    }
    public function GetChat(Room $room)
    {
        $user = Auth::user();
        if ($user && in_array($user->id, $room->Users->pluck('id')->toArray())) {
            $messages = getRoomMessages($room->id, $user->id);
            return view('admin.roomChat', compact('room', 'messages', 'user'));
        }
        return redirect('/');
    }
    public function SaveMessage(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'message' => 'max:500',
            'file' => 'mimes:pdf,jpg,jpeg,png,gif|max:10000000',
            'room_id' => 'required|exists:rooms,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json(['status' => 0, 'message' => $validate->errors()->first()]);
        }
        if (!$request->file && !$request->message) {
            return response()->json(['status' => 0, 'message' => 'no data to save']);
        }
        if (!Auth::check()) {
            $msg = trans('site.needLogin');
            session()->put('error', $msg);
            return response()->json(['status' => 2, 'message' => $msg]);
        }

        if ($request->file) {
            $filename = uploadImage($request->file, 'rooms/' . $request->room_id);
            $lastMessage = saveMessage($request->room_id, $filename, Auth::id(), 'file');
        } elseif ($request->message) {
            $lastMessage = saveMessage($request->room_id, $request->message, Auth::id());
        }
        return response()->json(['status' => 1, 'message' => 'success', 'data' => $lastMessage]);
    }
############################# END CHAT WORK #############################


}
