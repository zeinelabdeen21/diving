<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RoomsResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'private'           => (string) $this->private,
            'type'              => (string) $this->type,
            'order_id'          => (string) $this->order_id,
            'user_id'           => $this->user_id,
            'last_message_id'   => !is_null($this->last_message_id) ? $this->last_message_id:"",
            'last_message_body' => isset($this->LastMessage) ? ($this->LastMessage->type =='text' ? $this->LastMessage->body : 'file'):"",
            'last_message_type' => isset($this->LastMessage) ? $this->LastMessage->type:"",
        ];
    }
}
