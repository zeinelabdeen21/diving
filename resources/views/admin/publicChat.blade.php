@extends('layouts.app')

@push('style')
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet">
    <style media="screen">
        .online {
            color: #32CD32;
        }

        .ffside {
            height: 100%;
            position: fixed;
            z-index: 1;
            top: 0;
            right: 0;
            width: 18em;
            overflow-x: hidden;
            padding-top: 50px;
        }

        .chat_box {
            width: 260px;
            padding: 5px;
            position: fixed;
            bottom: 0px;
        }
    </style>
@endpush

@section('content')
    <public-component></public-component>
@endsection
